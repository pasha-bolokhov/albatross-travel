<?php
define('PayPal_Username', $_ENV['PAYPAL_USERNAME']);
define('PayPal_Password', $_ENV['PAYPAL_PASSWORD']);
define('PayPal_Signature', $_ENV['PAYPAL_SIGNATURE']);
define('PayPal_MerchantID', $_ENV['PAYPAL_MERCHANTID']);

define('PayPal_App_ClientID', $_ENV['PAYPAL_APP_CLIENTID']);
define('PayPal_App_Secret', $_ENV['PAYPAL_APP_SECRET']);
?>
