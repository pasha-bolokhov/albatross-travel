/**
 * Controls the 'home' page
 */
app.controller('HomeController', function($scope, $state) {

	$scope.interval = 4000;

	$scope.slides = [
		{ image: "images/xl_GreeceSunset.jpg", text: "Let the Horizon Come to You",
		  slogan: "The airport lights are already blinking", button: "Start your vacation" },
		{ image: "images/woman.jpg", text: "Office Work Brings you Nothing but Stress?",
		  slogan: "Dream yourself out of the office cube", button: "Get out now" },
		{ image: "images/albatross.jpg", text:"Albatross Travel \xA9",
		  slogan: "A Different Measure of Distance", button: "Hop on Our Back" },
	];

	$scope.go = function() {
		$state.go("guest.packagesRoot.packages");
	}
});
